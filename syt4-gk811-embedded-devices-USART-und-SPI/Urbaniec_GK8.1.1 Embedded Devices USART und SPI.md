# GK8.1.1 Embedded Devices "USART und SPI" 

## Kacper Urbaniec | 4AHIT | 19.03.2019

## Vorraussetzungen

* Windows-VM mit STM32-Workbench (`Windows10x64V`)

## Aufgabenstellung

1. Sieh dir das Beispiel uardExample1.c an und bringe es zum Laufen. Beschreibe das
   Programm in deinem Protokoll.
2. Sieh dir in diesem Beispiel an, wie mehrere Pins mit einem Funktionsaufruf konfiguriert
   bzw. auch gesetzt werden. Schreibe nun ein Programm, welches eine fixe gegebene Zahl (0-
   255) in zwei Teilen auf die 4 LEDS des Boards ausgibt. Eine Ausgabe soll jeweils mit dem
   Button angestoßen werden. Vor der Ausgabe sollen alle vier LEDs jeweils kurz einmal bzw. 2
   Mal blinken.
3. Sieh dir das Beispiel uartExample2.c an und bringe es zum Laufen. Beschreibe das
   Programm in deinem Protokoll. Füge einen Befehl z (n eine Zahl von 0-65535) hinzu.
   Diese Zahl soll dann auf den vier LEDs unseres STM-Boards in vier Teilen angezeigt werden.
4. Sieh dir das Beispiel spiExample.c an und bringe es zum Laufen. Hier geht es darum, die
   richtige Verdrahtung für das Schieberegister zu finden. Beschreibe das Programm in deinem
   Protokoll.
5. Erweitere das Beispiel so, dass du eine Ampel mittels der SPI-Verbindung steuerst.
   Schreibe (UART-) Kommandos, mit welchen du die Ampel steuerst, also Kommandos zum
   Starten und Stoppen sowie für die Tag-/Nachtumstellung.

## Implementierung

Nachdem ich meine VM gestartet habe, war der erste Schritt die Treiber für das *Serial Adapter Module* zu installieren. Diese kann man unter folgenden [Link](https://drive.google.com/folderview?id=0B6uNNXJ2z4CxX3U3RTYwc3RWQmM&usp=sharin
g) finden. Nachdem die Treiber installiert waren, konnte man schon den Adapter am PC anschließen und auswählen, dass er mit der VM verbunden werden soll. Anschließend konnte man im Geräte-Manager schauen, unter welchen seriellen Anschluss er verbunden wurde. Bei mir war es `COM5`. Für das Auslesen wird das Programm *Putty* empfohlen. Dieses kann man per Installateur aus dem Internet oder per Paket-Manager *Chocolatey* mit `choco install putty` installieren.

### Aufgabe 1

Zu aller erst musste das *Serial Adapter Module* mit dem Board verbunden werden. Das weiße Kabel musste am Port **PA2** verbunden werden, das gelbe Kabel an **PA3**. Als nächstes habe ich das Programm **uardExample1.c** in der Workbench geöffnet.  Im Programm wird eine UART (Universal Asynchronous Receiver Transmitter) - Schnittstelle definiert, diese dient zum Senden von Daten über eine Datenleitung, in unserem Fall über das **Serial Adapter Module**. Die Methode `USART2_GPIO_Configuration(void)` konfiguriert die Ports PA3 und PA2 für UART, die Ports werden dafür auf den Modus *Alternate Function Push Pull Mode* gesetzt. Die Methode `USART2_Configuration(void)` konfiguriert UART selber. Diese initialisiert den UART Block, wichtig dabei ist die *BaudRate* von 115200, diese muss dann in *Putty* eingegeben werden, um die Daten korrekt zu empfangen.    
In Beispiel sind folgende Zeilen meiner Meinung nach sehr wichtig:

```c
sprintf(str1, "Hello Welt - No. %d!\r\n", counter++);
rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
```

Die Methode`sprintf` speichert im ersten übergebenen Parameter, einem Pointer aus char element also einfach einem String, den nachfolgenden Text formatiert rein. Die zweite Methode `HAL_UART_Transmit` übergibt wie schon der Name sagt die UART Daten weiter. Der erste Parameter ist die Konfigurationsdatenstruktur, der nächste die Adresse, darauf folgt die Länge des Arrays/Strings, sowie ein zeitlicher Timeout am Schluss. 

In den daraufolgenden Bildern sieht man den Aufbau des Boards und die dazugehörige *Putty* für das Board.

<img src="img/1.jpg" width=145/><img src="img/1_2.png" width=400/> 

Jetzt konnte das Programm gestartet werden und mit *Putty* der Output beobachtet werden. Das Programm gibt einen Counter aus, der beim Reseten wieder von null anfängt.

<img src="img/1_3.png" width=260/>

### Quellcode

```c
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include <string.h>
#include <stdio.h>

UART_HandleTypeDef UartHandle;

/**
 * Configure GPIOs for USART2.
 * USART1 funktioniert nicht richtig, d�rfte bei STM32F407G-DISC1
 * f�r Bootloader verwendet werden, siehe Kap. 2.4 "Boot Configuration"
 * im im Reference Manual der CPU.
 * wei�er Draht(RX) mit PA2 (TX) verbinden
 * gelber Draht(TX) mit PA3(RX) verbinden
 * d.h. kruezweise - schreibendes Pin am Board mit lesendem Pin am PC
 * verbinden und vice versa.
 */
void USART2_GPIO_Configuration(void) {
	GPIO_InitTypeDef  GPIO_InitStruct;
	__GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_NOPULL;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

/**
 * Configurare USART2.
 */
void USART2_Configuration(void) {
	HAL_StatusTypeDef rc;
	USART2_GPIO_Configuration();
	__USART2_CLK_ENABLE();
	// Zusatz kopiert von Projekt usart2,
	//wenig Nutzen? etwas abge�ndert.
	UartHandle.Lock = HAL_UNLOCKED;
	UartHandle.gState = HAL_UART_STATE_RESET;
	UartHandle.RxState = HAL_UART_STATE_RESET;
	UartHandle.ErrorCode = HAL_USART_ERROR_NONE;

	  /*##-1- Configure the UART peripheral ######################################*/
	  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
	  /* UART configured as follows:
	      - Word Length = 8 Bits
	      - Stop Bit = One Stop bit
	      - Parity = None
	      - BaudRate = 9600 baud
	      - Hardware flow control disabled (RTS and CTS signals) */
	  UartHandle.Instance        = USART2;
	  //UartHandle.Init.BaudRate   = 1200;
	  //UartHandle.Init.BaudRate   = 9600;
	  UartHandle.Init.BaudRate   = 115200;
	  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	  UartHandle.Init.StopBits   = UART_STOPBITS_1;
	  UartHandle.Init.Parity     = UART_PARITY_NONE;
	  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	  UartHandle.Init.Mode       = UART_MODE_TX_RX;
	  UartHandle.Init.OverSampling	= UART_OVERSAMPLING_16;
	  rc = HAL_UART_Init(&UartHandle);
	  if(rc != HAL_OK) {
	    //Error_Handler();
		  BSP_LED_Toggle(LED3);
	  }
}

int main(void){
	// test();
	HAL_Init();
	/*BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);*/

	/* Enable the GPIO_LED Clock */
	 __HAL_RCC_GPIOD_CLK_ENABLE();
	GPIO_InitTypeDef  GPIO_InitStruct;
	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = 0xF000;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	// set all Port D pins to zero.
	// ODR - Output Data Register!!!!
	GPIOD->ODR = 0;

	// usart
	char str1[100];
	int counter = 0;
	int rc1;
	//HAL_Delay(1000);
	USART2_Configuration();

	for(;;){
		HAL_Delay(500);
		// set LED Pins of Port D (Pins 12-15) to '1's.
		GPIOD->ODR |= (0xD000);
		HAL_Delay(500);
		// set LED Pins of Port D (Pins 12-15) to '0's.
		GPIOD->ODR &= (!0xD000);

	    sprintf(str1, "Hello Welt - No. %d!\r\n", counter++);
	    rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
	    if (rc1 != HAL_OK) {
	    	BSP_LED_Toggle(LED3);
	    }
	}
}
```

### Aufgabe 2

Für die zweite Aufbage, sollte ein Programm geschrieben werden, welches eine fixe gegebene Zahl (0-255) in zwei Teilen auf die 4 LEDS des Boards ausgibt. Eine Ausgabe sollte jeweils mit dem Button angestoßen werden. 

Am Anfang sollte mal zuerst verstanden werden, wie die LEDs angesteuert werden können.

![](img/2_1.PNG)

Die ersten vier Bit einer Hexadezimalen Zahl steurn die Ports 12-15 an, wo sich die LEDs befinden. Doch wenn eine Zahl z.B. Hex 0x18 (= dec 24) nur die letzen 8 Bit einnimmt, dann sind die vorderen einfach mit 0 gefüllt. Und wenn die ersten vier 0 sind, dann leuchtet auch nichts. Deshalb müssen diese Stellen mit dem Bitshift-Operatoren nach links geschoben werden. Zuerst muss der erste Teil um 8 Bit, dann der zweite Teil um 12 Bit nach links geschoben werden. So kann die Zahl an den LEDs ausgebeben werden. 

Jede LED steht für eine Bit-Stelle, so sieht die Reihenfolge vom größten zum kleinsten Bit aus:    
blau - rot - orange - grün 

Beispiel:   
Zahl Hex. 0x18   
ist Dec 24   
ist Bin 0001 1000   
Ausgabe: grün - blau

Nachdem ich dies verstanden hatte, war die Programmierung kein Mysterium mehr. Mit `GPIOD->ODR &= (!0xF000);` können alle LEDs ausgeschaltet werden. Dabei steht und & für den Bitshift **und**-Operator, das **!** für ein logisches **nicht**. Gegenteilig können mit `GPIOD->ODR |= (0xF000);` alle LEDs eingeschaltet werden, dabei steht \| für den Bitshift **oder**-Operator. 

Für die Anzeige eines Wertes **num** muss man dann nur mehr Bits verschieben:

```c
int y = num<<8;
GPIOD->ODR |= (y);

int z = num<<12;
GPIOD->ODR |= (z);
```

Für die Knopf-Betätigung zum Starten der Anzeige habe ich Interrupt-Handler verwendet.

Für die Zahl 0x1f (=31) erhält man folgende Ausgabe nach dem Betätigen des Knopfes:

<img src="img/2_2.PNG" width=230/><img src="img/2_3.PNG" width=230/>

### Quellcode

```c
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include <string.h>
#include <stdio.h>

UART_HandleTypeDef UartHandle;

/**
 * Configure GPIOs for USART2.
 * USART1 funktioniert nicht richtig, d�rfte bei STM32F407G-DISC1
 * f�r Bootloader verwendet werden, siehe Kap. 2.4 "Boot Configuration"
 * im im Reference Manual der CPU.
 * wei�er Draht(RX) mit PA2 (TX) verbinden
 * gelber Draht(TX) mit PA3(RX) verbinden
 * d.h. kruezweise - schreibendes Pin am Board mit lesendem Pin am PC
 * verbinden und vice versa.
 */
void USART2_GPIO_Configuration(void) {
	GPIO_InitTypeDef  GPIO_InitStruct;
	__GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_NOPULL;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


/**
 * Configurare USART2.
 */
void USART2_Configuration(void) {
	HAL_StatusTypeDef rc;
	USART2_GPIO_Configuration();
	__USART2_CLK_ENABLE();
	// Zusatz kopiert von Projekt usart2,
	//wenig Nutzen? etwas abge�ndert.
	UartHandle.Lock = HAL_UNLOCKED;
	UartHandle.gState = HAL_UART_STATE_RESET;
	UartHandle.RxState = HAL_UART_STATE_RESET;
	UartHandle.ErrorCode = HAL_USART_ERROR_NONE;

	  /*##-1- Configure the UART peripheral ######################################*/
	  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
	  /* UART configured as follows:
	      - Word Length = 8 Bits
	      - Stop Bit = One Stop bit
	      - Parity = None
	      - BaudRate = 9600 baud
	      - Hardware flow control disabled (RTS and CTS signals) */
	  UartHandle.Instance        = USART2;
	  //UartHandle.Init.BaudRate   = 1200;
	  //UartHandle.Init.BaudRate   = 9600;
	  UartHandle.Init.BaudRate   = 115200;
	  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	  UartHandle.Init.StopBits   = UART_STOPBITS_1;
	  UartHandle.Init.Parity     = UART_PARITY_NONE;
	  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	  UartHandle.Init.Mode       = UART_MODE_TX_RX;
	  UartHandle.Init.OverSampling	= UART_OVERSAMPLING_16;
	  rc = HAL_UART_Init(&UartHandle);
	  if(rc != HAL_OK) {
	    //Error_Handler();
		  BSP_LED_Toggle(LED3);
	  }
}
int flag = 0;

// Interrupt-Exit-Handler
void EXTI0_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(KEY_BUTTON_PIN);
}
// Callback-Funktion
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_PIN) {
	if(flag == 0) {
		flag = 1;
	}
	else {
		flag = 0;
	}
}

int main(void){
	// test();
	HAL_Init();
	/*BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);*/

	/* Enable the GPIO_LED Clock */
	 __HAL_RCC_GPIOD_CLK_ENABLE();
	GPIO_InitTypeDef  GPIO_InitStruct;
	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = 0xF000;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	// set all Port D pins to zero.
	// ODR - Output Data Register!!!!
	GPIOD->ODR = 0;

	// usart
	char str1[100];
	int counter = 0;
	int rc1;
	//HAL_Delay(1000);
	USART2_Configuration();


	// Button als Interrupt-Trigger definieren
	__GPIOA_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.Pin = GPIO_PIN_0;
	GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	HAL_NVIC_SetPriority(EXTI0_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);

	// 0x18 -> 24
	int num = 0x1f;
	for(;;){
		if(flag == 1) {
			int y = num<<8;
			GPIOD->ODR |= (y);

			HAL_Delay(2000);
			// set LED Pins of Port D (Pins 12-15) to '0's.
			GPIOD->ODR &= (!0xF000);
			HAL_Delay(500);
			// set LED Pins of Port D (Pins 12-15) to '1's.
			GPIOD->ODR |= (0xF000);
			HAL_Delay(500);
			// set LED Pins of Port D (Pins 12-15) to '0's.
			GPIOD->ODR &= (!0xF000);
			HAL_Delay(500);
			// set LED Pins of Port D (Pins 12-15) to '1's.
			GPIOD->ODR |= (0xF000);
			HAL_Delay(500);
			// set LED Pins of Port D (Pins 12-15) to '0's.
			GPIOD->ODR &= (!0xF000);
			HAL_Delay(500);

			int z = num<<12;
			GPIOD->ODR |= (z);

			HAL_Delay(3000);
			GPIOD->ODR &= (!0xF000);
			flag = 0;

		}
	}
}
```

### Aufgabe 3

Diese Aufgabe basiert auf **uartExample2.c**. Im Grunde ist sie sehr ähnlich zur Aufgabe 1, es wird wieder auf UART gesetzt. Diesmal wird aber in der Methode **USART2_Configuration** auch ein Input definiert, so dass man per *Putty* Befehle an das Board senden kann. Das Programm selber besizt schon Funktionen, per Kurzbefehl wie **g** kann eine LED ein- und ausgeschaltet werden. 

Im Programm habe ich folgende if-Anweisung hinzugefügt. Diese schaut ob das erste Zeichen der Eingabe ein **z** für Zahl ist. Dies ist einfach, weil ein String einfach ein char-Array ist. Der nächste Schritt ist komplizierter, man muss den restlichen Buffer in einen  Integer umwandeln. Dazu gibt es zum Glück die Method `atoi` - Alpha to Integer. Diese wandelt einen String in eine Zahl um, als Parameter muss man dann die Eingabe minus der ersten Stelle geben (da diese ja das **z** ist). 

Für die Anzeige habe ich eine Methode `light` definiert, die per Bitshift wieder die LEDs anzeigt. Im Grunde genause wie in Beispiel 2, nur dass man am Anfang nicht shiftet, dann um 4, dann 8 und dann zum Schluss wieder um 12 Stellen nach links shiftet. 

```c
if (cmdBuffer[0]=='z') {
    int x = atoi((char*)(cmdBuffer+1));
    sprintf(str1, "Number given %d\r\n", x);
    rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
    light(x);
```

Die Ausgabe der Zahl 34952 (= Hex 8888) sieht dann so aus:

<img src="img/3_1.PNG" width=150/><img src="img/3_2.PNG" width=150/><img src="img/3_3.PNG" width=150/><img src="img/3_4.PNG" width=150/>

Eine wirklich lustige Zahl zum Testen.

### Quellcode

```c
#include "stm32f4xx.h" 
#include "stm32f4_discovery.h"
#include <string.h>
#include <stdio.h>

UART_HandleTypeDef UartHandle;
// Definitionen f�r Datenempfang
#define BUFFER_LENGTH 50
uint8_t receiveBuffer[BUFFER_LENGTH];
int receiveIndex;
uint8_t cmdBuffer[BUFFER_LENGTH];
int commandReady; // 0- nein, 1-ja


/**
 * Configure GPIOs for USART2.
 * USART1 funktioniert nicht richtig, d�rfte bei STM32F407G-DISC1
 * f�r Bootloader verwendet werden, siehe Kap. 2.4 "Boot Configuration"
 * im im Reference Manual der CPU.
 * wei�er Draht(RX) mit PA2 (TX) verbinden
 * gelber Draht(TX) mit PA3(RX) verbinden
 * d.h. kruezweise - schreibendes Pin am Board mit lesendem Pin am PC
 * verbinden und vice versa.
 */
void USART2_GPIO_Configuration(void) {
	GPIO_InitTypeDef  GPIO_InitStruct;
	__GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_NOPULL;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


/**
 * Configurare USART2.
 */
void USART2_Configuration(void) {
	HAL_StatusTypeDef rc;
	USART2_GPIO_Configuration();
	__USART2_CLK_ENABLE();
	// Zusatz kopiert von Projekt usart2,
	//wenig Nutzen? etwas abge�ndert.
	UartHandle.Lock = HAL_UNLOCKED;
	UartHandle.gState = HAL_UART_STATE_RESET;
	UartHandle.RxState = HAL_UART_STATE_RESET;
	UartHandle.ErrorCode = HAL_USART_ERROR_NONE;

	  /*##-1- Configure the UART peripheral ######################################*/
	  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
	  /* UART configured as follows:
	      - Word Length = 8 Bits
	      - Stop Bit = One Stop bit
	      - Parity = None
	      - BaudRate = 9600 baud
	      - Hardware flow control disabled (RTS and CTS signals) */
	  UartHandle.Instance        = USART2;
	  //UartHandle.Init.BaudRate   = 1200;
	  //UartHandle.Init.BaudRate   = 9600;
	  UartHandle.Init.BaudRate   = 115200;
	  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	  UartHandle.Init.StopBits   = UART_STOPBITS_1;
	  UartHandle.Init.Parity     = UART_PARITY_NONE;
	  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	  UartHandle.Init.Mode       = UART_MODE_TX_RX;
	  UartHandle.Init.OverSampling	= UART_OVERSAMPLING_16;
	  rc = HAL_UART_Init(&UartHandle);
	  if(rc != HAL_OK) {
	    //Error_Handler();
		  BSP_LED_Toggle(LED3);
	  }

	  	 // Conmfigure for reading
	  HAL_NVIC_SetPriority(USART2_IRQn, 2, 0);
	  HAL_NVIC_EnableIRQ(USART2_IRQn);
	  //USART1->CR1 &= ~USART_CR1_UE;
	  //USART1->CR1 |= USART_CR1_RXNEIE | USART_CR1_RE;
	  // RXNEIE - Read Data Register Not Empty Interrupt Enable
	  USART2->CR1 |= USART_CR1_RXNEIE;
	  //USART1->CR1 |= USART_CR1_UE;
	  receiveIndex = 0;
	  receiveBuffer[0] = 0;
	  commandReady = 0;
}


int main(void) {
	char str1[100];
	int counter = 0;
	HAL_StatusTypeDef rc1;

	HAL_Init();

	/* Enable the GPIO_LED Clock */
	 __HAL_RCC_GPIOD_CLK_ENABLE();
	GPIO_InitTypeDef  GPIO_InitStruct;
	/* Configure the GPIO_LED pins */
	GPIO_InitStruct.Pin = 0xF000;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	// set all Port D pins to zero.
	// ODR - Output Data Register!!!!
	GPIOD->ODR = 0;

	USART2_Configuration();

	for(;;) {
	    if (commandReady) {
	    	if (cmdBuffer[0]=='z') {
	    		int x = atoi((char*)(cmdBuffer+1));
	    		sprintf(str1, "Number given %d\r\n", x);
	    		rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
	    		light(x);
	    	} else if (strcmp(cmdBuffer, "r")==0) {
	    		BSP_LED_Toggle(LED5);
	    		strcpy(str1, "Command red executed.\r\n");
				rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
	    	} else if (strcmp(cmdBuffer, "g")==0) {
	    		BSP_LED_Toggle(LED4);
	    		strcpy(str1, "Command green executed.\r\n");
				rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
	    	} else {
				sprintf(str1, "Invalid Command: \"%s\".\r\n", cmdBuffer);
				rc1 = HAL_UART_Transmit(&UartHandle, (uint8_t *)str1, strlen(str1), 5000);
	    	}
	    	commandReady = 0;
	    }
	}
}

void light(int num) {
	GPIOD->ODR |= (num);

	HAL_Delay(2000);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '1's.
	GPIOD->ODR |= (0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '1's.
	GPIOD->ODR |= (0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);

	int zwei = num<<4;
	GPIOD->ODR |= (zwei);

	HAL_Delay(2000);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '1's.
	GPIOD->ODR |= (0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '1's.
	GPIOD->ODR |= (0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);

	int drei = num<<8;
	GPIOD->ODR |= (drei);

	HAL_Delay(2000);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '1's.
	GPIOD->ODR |= (0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '1's.
	GPIOD->ODR |= (0xF000);
	HAL_Delay(500);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);

	int vier = num<<12;
	GPIOD->ODR |= (vier);

	HAL_Delay(3000);
	// set LED Pins of Port D (Pins 12-15) to '0's.
	GPIOD->ODR &= (!0xF000);
	HAL_Delay(500);
}

void USART2_IRQHandler(void) {
	char x = USART2->DR;
	if (x >= 0x7F)  {
		// ignore
		return;
	}
	switch (x) {
	case '\n':
		//ignore
		break;
	case '\r':
		if (commandReady) {
			// ignore command
		} else {
			strcpy(cmdBuffer, receiveBuffer);
			commandReady = 1;
		}
		receiveIndex = 0;
		receiveBuffer[receiveIndex] = 0;
		break;
	default:
		if (receiveIndex >= BUFFER_LENGTH-1) {
			if (commandReady) {
				// ignore command
			} else {
				strcpy(cmdBuffer, receiveBuffer);
				commandReady = 1;
			}
			receiveIndex = 0;
		}
		receiveBuffer[receiveIndex++] = x;
		receiveBuffer[receiveIndex] = 0;
		break;
	}
}

```

### Aufgabe 4

Bei der Aufgabe wird eine Ampel durch das Programm **spiExample.c** mithilfe eines Chips gesteurt. Das Programm benutzt SIP, das Serial Peripheral Interface zur Steurung der Dioden. Beim SIP-Protokoll gibt es immer einen Master und einen Slave, der Master ist in diesem Fall unser Board, der Slave der Chip von Texas Instruments. Die Methode `spiGpioInit(void)` koniguriert die GPIO-Pins für diesen Chip. Die Methode `spiInit(void)` konfiguriert SIP und bildet eine Datenstruktur **SpiHandle**, diese muss beim Transferieren der Daten auch benutzt werden. Die Methode `test1()` bringt die Lampen in der Reihenfolge rot gelb grün zum Leuchten. Die Umsetzung erfolgt über eine Datenstruktur **data**, diese gibt an welche Lampe leuchten soll. Sie wird durch eine Bitshift-Verschiebung manipuliert, um verschiedene Lampen zum Leuchten zu bringen. Das Senden der Daten an den Chip selber wird in einer unter-Methode `writeByte(uint8_t data)` ausgefürt. Dort wird die Datenstruktur **data** per `rc1 = HAL_SPI_Transmit(&SpiHandle, &data, 1, 5000);` an den Chip transferiert. 

Wenn man jetzt das Programm ausführt, sieht die Anzeige ungefähr so aus:

<img src="img/4_1.PNG" width=195/><img src="img/4_2.PNG" width=190/><img src="img/4_3.PNG" width=190/>

### Quellcode

```c
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);
static void Timeout_Error_Handler(void);

/* SPI handler declaration */
SPI_HandleTypeDef SpiHandle;

void spiGpioInit(void) {
	GPIO_InitTypeDef  GPIO_InitStruct;
	__GPIOC_CLK_ENABLE();
    /*##-2- Configure peripheral GPIO ##########################################*/
    /* SPI SCK GPIO pin configuration  */
    GPIO_InitStruct.Pin       = GPIO_PIN_10;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* SPI MISO GPIO pin configuration   MISO: Master in - Slave out */
    GPIO_InitStruct.Pin       = GPIO_PIN_11;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* SPI MOSI GPIO pin configuration  MOSI: Master out - Slave in */
    GPIO_InitStruct.Pin       = GPIO_PIN_12;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    // outputmode f�r SRCLK (shift register clock
    GPIO_InitStruct.Pin       = GPIO_PIN_9;
    GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = 0;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

void spiInit(void) {
	__SPI3_CLK_ENABLE();
	spiGpioInit();
	SpiHandle.Instance               = SPI3;
	SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	SpiHandle.Init.Direction         = SPI_DIRECTION_1LINE;//SPI_DIRECTION_2LINES;
	SpiHandle.Init.CLKPhase          = SPI_PHASE_1EDGE;
	SpiHandle.Init.CLKPolarity       = SPI_POLARITY_LOW;
	SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLED;
	SpiHandle.Init.CRCPolynomial     = 7;
	SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
	SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
	SpiHandle.Init.NSS               = SPI_NSS_SOFT;
	SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLED;
	//SpiHandle.Init.NSSPMode          = SPI_NSS_PULSE_DISABLED;
	//SpiHandle.Init.CRCLength         = SPI_CRC_LENGTH_8BIT;
	SpiHandle.Init.Mode = SPI_MODE_MASTER;
	if(HAL_SPI_Init(&SpiHandle) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}
}


void writeByte(uint8_t data) {
	HAL_StatusTypeDef rc1;
	rc1 = HAL_SPI_Transmit(&SpiHandle, &data, 1, 5000);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
	int i = 0;
	while(i<10)i++;
	HAL_Delay(20);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
	//HAL_Delay(100);
	//data = ~data;
	switch (rc1) {
	case HAL_ERROR:
		Error_Handler();
		break;
	case HAL_TIMEOUT:
		Timeout_Error_Handler();
		break;
	}
}


/**
 * Bringt in einer Reihenfolge rot-orange-gr�n usw zum Leuchten, falls die Lampen mit
 * den richtigen Pins verbunden sind (rot-QA, gelb-QB, gr�n-QC).
 */
void test1() {
	uint8_t data = 0x04;
	for(;;) {
		BSP_LED_Toggle(LED4);
		//HAL_SPI_TransmitReceive(&SpiHandle, (uint8_t*)aTxBuffer, (uint8_t *)aRxBuffer, BUFFERSIZE, 5000);
		writeByte(data);
		data = data<<1;
		if (data>4) {
			data = 1;
		}
		HAL_Delay(1000);
	}
}

void test2() {
	uint8_t data = 0xff;
	for(;;) {
		BSP_LED_Toggle(LED4);
		writeByte(data);
		data = ~data;
		HAL_Delay(1000);
	}
}

int main(void) {
	HAL_Init();
	BSP_LED_Init(LED4);
	BSP_LED_On(LED4);
	BSP_LED_Init(LED3); // ORANGE
	BSP_LED_Off(LED3);
	BSP_LED_Init(LED5);
	BSP_LED_On(LED5);
	spiInit();
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
	//SystemClock_Config();
	// Endlosschleife, kehrt nie mehr zur�ck.
	test1();
	// auch eine Endlosschleife
	//ampel1();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* Turn LED5 on */
  BSP_LED_On(LED5);
  while(1)
  {
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Timeout_Error_Handler(void)
{
  /* Toggle LED5 on */
  while(1)
  {
    BSP_LED_On(LED5);
    HAL_Delay(500);
    BSP_LED_Off(LED5);
    HAL_Delay(500);
  }
}
```



## Quellenverzeichnis

* [Treiber](https://drive.google.com/folderview?id=0B6uNNXJ2z4CxX3U3RTYwc3RWQmM&usp=sharing)
* [Dezimal zu Hex Tabelle](https://ascii.cl/conversion.htm)
* [Das Serial Peripheral Interface (SPI)](https://elearning.tgm.ac.at/pluginfile.php/89252/mod_resource/content/1/spi.pdf)
* [USART-Interface beim Board STM32F407G-DISC1](https://elearning.tgm.ac.at/pluginfile.php/89251/mod_resource/content/1/usart.pdf)