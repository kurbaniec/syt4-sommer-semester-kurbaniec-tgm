# "*GEK8.2.2 Control in Linux iptables*" 

## Kacper Urbaniec | 06.05.2019 | 4AHIT

## Host-Based

### Aufgabenbeschreibung

Installieren Sie in ihrem (Linux) System einen Webserver und einen SSH-Server.

* Konfigurieren Sie eine Firewall (`iptables` oder `nftables`) so, dass nur der Zugriff auf diese Dienste (SSH, HTTP, HTTPS) von außerhalb des Systems erlaubt ist. Innerhalb des Systems (d.h. über `localhost` sollen alle Dienste erlaubt sein. Die Installation von Updates innerhalb des Systems muss weiterhin möglich sein. Verwenden Sie - soweit möglich - stateful Regeln.

### Implementierung

Installieren von iptables und netfilter (zum Abspeichern benötigt):

```bash
sudo apt-get install iptables-persistent netfilter-persistent
```

Regeln anwenden:

```bash
sudo iptables -F
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P INPUT DROP
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo netfilter-persistent save
```

Bedeutung der Parameter vom Befehl iptables:

* `-F` - Entfernt alle vorhandenen Regeln
* `-P` - Legt eine Policy für eine Chain fest, falls keine Filterregel zutrifft (Somit einen Defaultwert)
* `-A` - Regel wird an die Kette der Chain hinzugefügt, erweitert (`-P`)
* `-i` - Paket wird nur geprüft, wenn es auf die eingehende Netzwerkschnittstelle eingegangen ist
* `-j` - Aktion, die auf ein Paket angewendet werden kann (`ACCEPT | DROP | REJECT | LOG`)
* `-m` - Ladet ein Modul, z.B. `state` um Befehle wie `--state` zu ermöglichen, welches schaut ob ein Paket gewisse Kriterien erfüllt wie `established`
* `-p` - Paket wird nur geprüft, wenn es die IP-Protokoll Richtlinien erfüllt
* `--dport` - Paket wird nur geprüft, wenn es an den gegebenen Port gesendet wird, benötigt `-p` zwingends

Befehl zum Anzeigen derzeitiger Regeln:

```bash
sudo iptables -S
```

Nach dem Anwenden der Regeln wurden Aktionen wie Pings wie erwartet von der Firewall blockiert.

![ping](img/ping.PNG)

SSH und HTTP konnten aber erfolgreich getestet werden. Für HTTPS bräuchte man valide Zertifikate für Apache, in der Theorie sollte es aber funktionieren.

![ssh](img/ssh.PNG)

![http](img/http.PNG)

## Network Based (DMZ)

### Aufgabenbeschreibung

Konfigurieren Sie zwei virtuelle Maschinen wie folgt: `(Internet/TGM) --- (Router) --- (Webserver)`.

* Für das Netzwerk zwischen Router und Webserver sollen private Adressen verwendet werden. Konfigurieren Sie den Router so, dass der Zugriff auf `Router-IP:80` aus dem Internet/TGM-Netz auf den Webserver weitergeleitet wird (DNAT) und sonst keinerlei Verkehr vom/zum Webserver aus erlaubt ist (auch kein Zugriff auf HTTP, DNS, ...).

### Implementierung

Für diese Aufgabe fand ich einen guten [Forumsbeitrag](https://askubuntu.com/questions/764946/confiruration-dnat-in-iptables). Dieser erklärt wie man die iptables benutzen kann, um einen Art Router zu bilden. 

Dazu muss man folgende Befehle eingeben, wobei `192.168.60.128:80` die Adresse meines Webserver ist, denn ich vom letzten Beispiel weiterverwende. `192.168.60.130` ist die Adresse der VM, die den Router bildet.

```bash
sudo iptables -t nat -A POSTROUTING -o ens33 -j SNAT --to 192.168.60.130
sudo iptables -t nat -A PREROUTING -p tcp -i ens33 --dport 80 -j DNAT --to-destination 192.168.60.128:80
```

Die Regeln funktionieren so, dass die "Router"-VM mittels der NAT-Tabelle (`-t nat`) eine Adresse bei einem eingehenden Paket mit Port 80 mittels Destination-NAT (DNAT) an den Webserver weiterleitet. Die Antwort von diesem wird dann mittels Source-NAT (SNAT) auf die Router-IP übersetzt und zurückgegeben. Dies ist im Normalfall benötigt, weil man zwischen öffentlichen und privaten Adressen in einem Netzt unterscheiden muss.

![iptables - NAT](img/nat.PNG)

Mitfolgenden Befehl kann die Filter-Tabelle für NAT eingesehen werden:

```bash
sudo iptables -t nat -nvL
```

Doch beim Testen von `192.168.60.130` wurde nichts angezeigt.

Nach langer Zeit fand ich in einem Forum die [Lösung](https://askubuntu.com/questions/167819/im-getting-fsync-failed-error-why). Beim Netzwerk-Interface `ens33` ist standardmäßig keine Weiterleitung aktiviert, man muss diese erst aktivieren. Zum testen, ob sie aktiviert ist, muss man nur `cat /proc/sys/net/ipv4/conf/ens33/forwarding` ausführen. Wenn 0 herauskommt ist sie deaktiviert, was bei mir der Fall war. Mann muss diesen Wert auf 1 setzen:

```bash
echo '1' | sudo tee /proc/sys/net/ipv4/conf/ens33/forwarding
echo '1' | sudo tee /proc/sys/net/ipv6/conf/ens33/forwarding
```

Jetzt funktionierte die Anzeige:

![webserver](img/router.PNG)

Zum testen pausierte ich den Webserver per VMware und die Umleitung funktionierte logischerweise nicht, weil der Server ja nicht erreichbar dadurch ist. Doch beim wieder hochfahren funktionierte der Webserver nicht mehr, das Interface `ens33` fehlte komplett.

Dieses musste ich neue aufsetzen:

```bash
sudo vim /etc/network/interfaces
```

```bash
# Add this
auto ens33
iface ens33 inet dhcp
```

```bash
sudo ifup ens33
```

Dann funktionierte alles wieder.

Damit der Router aus dem Internet erreichbar ist, muss man in VMware Port-Forwarding verwenden, so kann der lokale Port 80 des Host-Rechners auf den der Router VM gemappt werden. Dazu geht man auf `Edit` - `Virtual Network Editor`.

![Router conf](img/RouterConfig.PNG)

Jetzt kann auf den Router lokal oder von außen zugegriffen werden:

![router www](img/routerWWW.PNG)

## Zugriffsbeschränkungen mit AppArmor/SELinux

### Aufgabenbeschreibung

* Konfigurieren Sie Zugriffsbeschränkungen auf Ihrem System mittels `AppArmor` und/oder `SELinux` so, dass der Zugriff auf das Web-Verzeichnis (`/var/www/html` oder ähnlich - je nach Webserver und Distribution) für den Benutzer des Webservers (`www-data` o.ä.) NICHT erlaubt ist, wohl aber für den Dienst `httpd` bzw. `nginx`. Ist eine derartige Konfiguration überhaupt möglich - warum bzw. warum nicht? Ergänzen Sie die Zugriffsbeschränkungen des Webservers so, dass dieser NUR die beiden Ports `80` und `443` öffnen darf.
* Erstellen Sie ein Programm (in beliebiger Programmiersprache), welches die beiden Dateien `/tmp/data` und `/tmp/secure` anlegen soll. Konfigurieren Sie Zugriffsbeschränkungen so, dass das Programm NUR die Datei `/tmp/data` anlegen darf, sonst nichts.

### Implementierung

AppArmor Installation:

```bash
sudo apt-get install apparmor apparmor-utils 
```

**Aufgabe 1**

In AppArmor habe ich keinen Weg gefunden, um Verzeichnisse vor User zu schützen. 

#### Option 1

Man kann aber mittels der Rechteverwaltung von Linux den Inhaber des Verzeichnis auf den Webserverbenutzer (`www-data`) ändern und dann mittels chmod allen anderen Bneutzern  die Rechte entziehen. 

```
sudo chown -R www-data /var/www/html
sudo chmod o-rwx /var/www/html
```

Dadurch kann kein andere Benutzer mehr Änderungen durchführen. Vollends sinnvoll ist die Konfiguration nicht ganz, wenn man aber eine fertige Website besitzt die auf keinen Fall geändert werden soll, dann kann man vielleicht so eine extreme Konfiguration durchführen.

![chmod](img/chmod.PNG)

#### Option 2

Man kann aber mittels AppArmor Apache den Ordner `var/www/html` verwehren. Ist nicht wirklich die Aufgabenstellung, aber eine Wissenserweiterung für AppArmor.

Man geht in den Ordner `etc/apparmor.d`. Dort gibt man folgende Kommandos ein, um ein Profile für Apache (Service-Name `apache2`) zu erstellen.

```bash
sudo aa-autodep apache2
sudo aa-complain apache2 
```

Jetzt muss das Profil editiert werden (` sudo vim usr.sbin.apache2`) und ein *deny*-Anweisung für `var/www/html` hinzugefügt werden.

```bash
# Last Modified: Tue May 14 01:50:36 2019
#include <tunables/global>

/usr/sbin/apache2 {	
  #include <abstractions/base>

  /lib/x86_64-linux-gnu/ld-*.so mr,
  /usr/sbin/apache2 mr,
  deny /var/www/html r,

  ^DEFAULT_URI {
  }

  ^HANDLING_UNTRUSTED_INPUT {
  }
}
```

Jetzt wendet man die Regel per `sudo aa-enforce apache2` an und startet AppArmor und Apache neu (`sudo /etc/init.d/apparmor reload; sudo service apache2 restart`).

Jetzt kann Apache nicht mehr gestartet werden, weil der Ordner gesperrt wurde.

![Apache](img/apache2.PNG)

Solch eine Konfiguration ist aber nicht wirklich sinnvoll. Allgemein finde ich, dass der Besitzer immer Zugriff auf Pfade wie `var/www/html` haben sollte, nicht nur der Dienst. Wenn der User von  `var/www/html` ausgeschlossen wird, kann man keine Dateien mehr einfügen, außer man hat eine Website auf Basis von beispielweise Wordpress, wo man Dateien durch die Seite selber hochladen kann. 

Wenn man die Zugriffeinschränkungen auf nur die Ports `80` und `443` einschränken möchte, nimmt man die iptables-Konfiguration aus der ersten Aufgabe *Host-Based* und entfernt die Regel für SSH.

**Aufgabe 2**

Im Verzeichnis `~/armor_test` erstellte ich ein kleines Shell-Script names `armor_test.sh`:

```sh
#!/bin/bash 

echo "This is an apparmor example." 

touch /tmp/data
echo "file data created" 

touch /tmp/secure
echo "file secure created"

rm /tmp/data
echo "data deleted"

rm /tmp/secure
echo "secure deleted"
```

Mittels ` sudo aa-genprof armor_test.sh ` wird eine AppArmor-Profil kreiert. Dabei muss man aber währenddessen in  einem anderen Fenster nachdem Befehl das Script laufen lassen. Wenn dieses beendet ist, geht man zurück zum AppArmor-Fenster und drückt `s`.

Danach werden Fragen gestellt, ob AppArmor diese Aspekte des Skripts zulassen soll. Wir sagen bei jedem Ja, weil wir nicht für jedes File eine Ausnahme erstellen können, sondern nur für den gesamten `/tmp`- Ordner.

![1](img/1.PNG)

*Inherit*

![2](img/2.PNG)

*Allow*

![3](img/3.PNG)

*Allow*

Jetzt können wir das Profil händisch bearbeiten. Dazu öffnen wir mit `sudo vim /etc/apparmor.d/home.kurbaniec.armor_test.armor_test.sh` das Profil. In diesem fügen wir die Zeile `deny /tmp/secure w,`, damit sagen wird, dass das Script dieses File nicht erstellt werden darf.

```
/home/kurbaniec/armor_test/armor_test.sh flags=(complain) {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/consoles>
  #include <abstractions/user-tmp>

  /bin/bash ix,
  /bin/rm mrix,
  /bin/touch mrix,
  /home/kurbaniec/armor_test/armor_test.sh r,
  /lib/x86_64-linux-gnu/ld-*.so mr,
  deny /tmp/secure w,
}
```

Jetzt muss dieses Konfiguration neu validiert werden, um in Kraft zu treten. Dazu wird `sudo apparmor_parser -r /etc/apparmor.d/home.kurbaniec.armor_test.armor_test.sh` verwendet.

Das Ausführen von `armor_test.sh` führt zum erhofften Ergebnis:

![armor](img/armor.PNG)

## Quellen

* [iptables - Übersicht](https://www.digitalocean.com/community/tutorials/iptables-essentials-common-firewall-rules-and-commands)
* [iptables - Ubuntu Wiki](https://wiki.ubuntuusers.de/iptables2/)
* [iptables - Man Page](https://linux.die.net/man/8/iptables)
* [iptables - Weiterleitung](https://www.karlrupp.net/de/computer/nat_tutorial)
* [Traffic im Localhost erlauben](https://serverfault.com/questions/843819/apt-get-not-working-with-iptables)
* [iptables - Router](https://askubuntu.com/questions/764946/confiruration-dnat-in-iptables)
* [Interface - Weiterleitung](https://askubuntu.com/questions/167819/im-getting-fsync-failed-error-why)
* [Re-add ens33](https://askubuntu.com/questions/793214/unable-to-access-internet-on-vmware)
* [AppArmor - Tutorial](https://medium.com/information-and-technology/so-what-is-apparmor-64d7ae211ed)
* [AppArmor - Block user](https://www.techrepublic.com/article/how-to-use-apparmor-to-block-access-to-folders-in-nginx/)

