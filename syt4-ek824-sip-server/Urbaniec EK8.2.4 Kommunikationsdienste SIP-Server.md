# "*EK8.2.4 Kommunikationsdienste SIP-Server*"

## Kacper Urbaniec | 4AHIT | 03.06.2019

## Voraussetzungen

* Grundlegende Kenntnisse SIP, SDP u. RTP  (SIP-Infrastruktur aus vorheriger Übung)

## Aufgabenstellung

1. Es soll SIP-Protokoll der Registrierung mit Wireshark aufgezeichnet und Flow-Chart erstellt werden.
2. Stellen Sie eine Sprachverbindung zwischen den beiden UAs her. Beschreiben Sie einen Verbindungsaufbau über SIP-Server und zeichnen Sie den Ablauf auf (verwenden Sie dabei Wireshark). Welche TCP/IP-Ports werden für die Signalisierung verwendet? Welche TCP/IP-Ports werden für den Mediastream zwischen den UA verwendet? Welche Komponenten sind dabei beteiligt? 
3. Es soll RTP mit Wireshark erfasst und analysiert werden. Welcher Codec wird verwendet. 
4. Erstellen Sie ein geeignetes Szenario, um NAT-Traversal zu untersuchen

## Implementierung 

Für die Übung benutze ich drei VMs. Eine Debian-VM mit der Adresse `192.168.60.133`, auf dieser läuft der SIP-Server "Kamailio". Dann habe ich zwei Ubuntu-VMs, die als Clients fungieren. So gibt es die VM mit der Adresse `192.168.60.131`, auf der der User `user1` verwendet wird und die VM mit `192.168.60.132`, auf der `user2` verwendet wird. Als Software zum Testen wird auf den Clients "Linphone" verwendet.

Zum Erfassen der Übertragungen wird das Programm "Wireshark" verwendet. In diesem wählt man die Netzwerkschnittstelle von VMware, bei mir VMnet8, um die Übertragungen zwischen den virtuellen Maschinen beobachten zu können.

### Aufgabe 1

Um eine Registrierung zu beobachten startet man einfach den SIP-Server und danach einzeln die Clients, die per Linphone sich registrieren. Beim Registrieren übermitteln die Clients die genauen Adress-Informationen des Benutzers an den Server, das bedeutet sie melden sich bei ihm an. Bei erfolgreicher Registrierung kommt eine Status von 200 zurück. Im Bild unten sieht man nicht nur die Registrierung, sondern auch die Anmeldung für Notifikationen vom SIP-Server, die durch Subscribe erkennbar sind.

![](img/1.PNG)

Um einen Flowchart in Wireshark zu bilden geht man auf den Menüpunkt `Statistiken` - `Flowgraph`. Es tauch eine Art Druckfenster auf, indem man noch nach Anzeigefilter die Ausgabe einschränken kann. Beim Speichern muss man bisschen aufpassen, nur der Bereich der im Fenster gerade sichtbar ist, wird exportiert. 

Die Flowchart-Ausgabe für die Registrierung (und Subsription) sieht im Detail so aus. In Der Mitte ist der SIP-Server platziert, an dem die Clients anfragen schicken und vice versa.

![](img/Registrierung.PNG)

### Aufgabe 2

Um einen Verbindungsaufbau herzustellen, ruft einfach ein Client den anderen an. Dabei kommt es zu einem Anruf, indem die Verbindung veranschaulicht wird. Dazu habe ich wieder einen Flowgraph erstellt, der nur die SIP-Pakete anzeiget. 

`user1` will `user2` beim Zeitpunkt 20.14 anrufen, es wird ein SIP-Request an den Server gesendet, der diese an `user2` weiterleitet. Es folgen eine Zeit die SIP-Statuse 100 Trying und 180 Ringing, das heißt, dass der Empfänger kontaktiert werden konnte, aber die Anfrage noch nicht bearbeitet hat. Erst bei beim Zeitpunkt 28 hebt dieser denn Anruf ab, somit wird der erfolgreiche Status 200 verschickt. Darauf folgt die gegenseitige positive Bestätigung, die mittels Request mit ACK, also acknowledge, durchgeführt wird. Ab dann bis zum Zeitpunkt 44.7 erfolgt die Kommunikation zwischen den Clients direkt mittels RTP, der Server wird kurz nicht benötigt. Erst dann wird die Sitzung durch wieder eine Request beendet, die aber mit Bye betitelt ist. Auf diese antworten die Clients im Normalfall mit einem positiven Status 200.

![con](img/Verbindung.PNG)

Grundlegen besteht somit eine SIP-Transaktion aus mehreren *SIP-Requests* und *SIP-Responses*.

Eine SIP-Request besteht immer aus einer Request Line, die folgende drei Elemente beinhaltet.

* Request Methode   
  Kennzeichnet den Request Typ (INVITE, ACK, BYE). Am Anfang beim Aufbau wird ein INVITE werden, später wird dieser per ACK wahrgenommen. Schlussendlich wird die Sitzung durch Bye beendet. 
* Request URL    
  Adresse,  an den die Anfrage (Request) gerichtet ist. Also wenn `user1` eine Request an `user2` sendet, dann wird in diesem Bereich beispielsweise `sip:user2@sip.syt.tgm.ac.at` stehen.
* SIP Version    
  Die Version wird deshalb immer mitgegeben, damit Gesprächspartner ihre Nachrichten gegenseitig korrekt interpretieren können. In unserem Fall ist das *SIP/2.0*. 

Eine Request sieht in Wireshark so aus, bei der Request Line erkennt man die gerade beschriebenen Elemente. Außerdem kann man die Port **5060** für jeweils den Src.- und Dst. Port feststellen.

![req](img/siprequest.PNG)



Das Gegenstück zu den SIP-Request sind die *SIP-Responses*. Sie signalisieren dem Server, dass eine Request erhalten wurde. Vom Aufbau her ähneln sie stark der Request, anstatt der Request Line haben sie logischerweise eine Status Line die folgende Elemente beinhaltet:

* SIP Version   
  Wird wie bei den Anfragen verwendet.

* Status Code   
  Beschreibt die Rückmeldung auf die SIP-Request. zum Beispiel wird bei Erfolg ein Status von 200, für OK, zurückgegeben.   Jede Response gibt also einen Status, um wie es um die Verbindung steht. Es gibt sechs Arten von möglichen Status:

  | 1xx         | 2xx         | 3xx         | 4xx          | 5xx          | 6xx            |
  | ----------- | ----------- | ----------- | ------------ | ------------ | -------------- |
  | Information | Erfolgreich | Redirection | Client Error | Server Error | Global Failure |

* Reason Phrase   
  Ist die textuelle Beschreibung des Status Codes, also OK für 200 oder Trying für 180.

  | (Trying, Ringing) | (OK) |      | (Not found) | (Gateway timeout) |      |
  | ----------------- | ---- | ---- | ----------- | ----------------- | ---- |
  |                   |      |      |             |                   |      |


Auch bei den Responses sieht man wieder in Wireshark den Port 5060.

![trying](img/trying.PNG)



Bei SIP erfolgt der Medienstream durch eine Direktverbindung zwischen den Clients. Dazu wird wird RTP, dass den Transport von Audio- und Videodaten ermöglicht. Die ganze Nutzdatenübertragung wird über RTCP abgewickelt, was als eine Art Kontrollorgan fungiert. 

Für RTP wird der Port 7078 verwendet.

![rtp](img/rtp.PNG)

Für RTCP dagegen Port 7079.

![rtp](img/rtcp.PNG)



### Aufgabe 3

Das Real-time Transport Protocol (RTP) ist ein Netzwerkprotokoll zur Bereitstellung von Audio und Video über IP-Netzwerke. RTP wird in Kommunikations- und Unterhaltungssystemen mit Streaming-Medien eingesetzt, wie z.B. Telefonie oder Videoübertragungen.

RTP läuft typischerweise über UDP, da eine gesicherte Datenübertragung wie TCP bei Echtzeit-Anforderungen zu Verzögerungen führen würde, was nicht gewünscht ist. RTP wird in Verbindung mit dem RTP Control Protocol (RTCP) verwendet. Während RTP die Medienströme (z.B. Audio und Video) überträgt, wird RTCP zur Überwachung von Übertragungsstatistiken und Quality of Service (QoS) verwendet und unterstützt die Synchronisation mehrerer Streams. RTP ist eine der technischen Grundlagen von Voice over IP und wird in diesem Zusammenhang oft in Verbindung mit einem Signalisierungsprotokoll wie dem Session Initiation Protocol (SIP) verwendet, das Verbindungen über das Netzwerk herstellt.

Mit Wireshark erkennt man schön den Übergang zwischen SIP und RTP.

![siprtp](img/siprtp.PNG)

Bei RTP werden die Steuerinformationen wie Audio-Codecs hinterlegt. Diese sind im Payload-Parameter zu finden. 

![opus](img/opus.PNG)

Man erkennt, dass der Codec "*Opus*" verwendet wird. Mit diesen Namen kann man im Internet nach weiteren Informationen suchen. Ich habe auf Wikipedia ein paar technische Details gefunden.

![op1](img/op1.PNG)

![op1](img/op2.PNG)

## Quellen

* [Wireshark Download](https://chocolatey.org/packages/wireshark)
* [WinPcap - Benötigt damit Wireshark Netzwerkkomponenten erkennt](https://www.winpcap.org/install/)
* [Wireshark Flow Graph](https://www.techrepublic.com/blog/linux-and-open-source/using-the-flow-graph-feature-on-wireshark/)
* [SIP - Wikipedia](https://en.wikipedia.org/wiki/Session_Initiation_Protocol)
* [Opus Codec](https://en.wikipedia.org/wiki/RTP_audio_video_profile)
* [RTP - Wikipedia](https://en.wikipedia.org/wiki/Real-time_Transport_Protocol)
* Labor Nachrichtentechnik Versuch 1: Session Initiation Protocol (SIP)  - Dipl. Ing. Wolfgang Lautenschager