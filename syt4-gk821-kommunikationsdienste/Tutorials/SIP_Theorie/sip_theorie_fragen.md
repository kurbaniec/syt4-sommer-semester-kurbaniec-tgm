# Session Initiation Protocol (SIP)

## Fragen
1. Was ist der grundlegende Unterschied zwischen einer Telefonverbindung im heutigen
Telekommunikationsnetz und einer VoIP-basierten Telefonverbindung?  
Eine Festnetzverbindung setzt auf ein durch meist Kupfer-Verbindungen realisiertes Telefonnetz auch PSTN (Public Switched Telephone Network) genannt. Es benötigt eine 64 KBPS Verbindung und kann nur Ton übertragen, meist sind Übertragungen gebührenpflichtig.  
VoIP (Voice over IP) bezeichnet das Telefonieren (und viele weitere Funktionen z.B. Videochats) über das Internet. Man benötigt daher nur eine Internet-Verbindung und kann daher heutzutage auch dadurch mobil und portabel sein. Wie vorher gesagt unterstützt es mehr Funktionen und ist deutlich kostengünstiger. Die Standard-VoIP Telefonie setzt ungefähr auf 10 KPBS.

2. Welcher gravierender Unterschied besteht zwischen HTTP und SIP in Bezug auf die
Client-Server Architektur, wenn man Endgerät (Telefon bzw. PC) und Netzwerkknoten
(Proxy bzw. Webserver) betrachtet. Welche Funktionalitäten können im Fall HTTP bzw.
SIP im Endgerät vorhanden sein und welche im Netzwerkknoten?   
SIP:  
Bei SIP werden die Proxys zum Verbindungsaufbau benötigt. Der Proxy, von dem der Verbindungsaufbau ausgeht wird als Outbound-Proxy bezeichnet, der der die Verbindung annimmt wird Inbound-Proxy genannt. Nachdem Verbindungsaufbau kommunizieren die Clients direkt miteinander, die Proxys werden dann nicht mehr benötigt.  
Proxy-A <-------------> Proxy-B  
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ||   
SIP-Client-A <---> SIP-Client-B  
&nbsp;  
HTTP:   
Bei HTTP werden Proxys eingesetzt um Anfragen, meist vom Client, an den Server weiterzuleiten. Natürlich können mehr Clients Anfragen senden. Theoretisch muss der Proxy nicht als Netzwerkkomponente aufgebaut werden, sondern kann auch rein softwaretechnisch umgesetzt werden.   
Client <---> Proxy <---> Server

3. Wie erfolgt die Kommunikation zwischen Client und Server und was kann dabei
abgehandelt werden?  
Der Client kann einen Dienst vom Server anfordern. Der Server, der sich im selben Netz befindet, stellt dann die Betriebsmittel bereit. Ein Server ist meist für mehrere Clients verantwortlich. (= Client-Server-Modell)  

4. Aus welchen Teilen besteht eine SIP- (oder auch HTTP-) Nachricht?  
Ein SIP-Request und Response bestehen aus textuellen Beschreibungen, jede SIP-Nachricht besteht aus einer Start-Line, Header und einem Body:

| Start-Line       | >           | Request-Line oder Status-Line (SIP-Version, ..)  |
| ------------- |-------------| -----:|
| Message-Header     | > | Message Originator/Receiver Message-Route (Via) usw. (Parameter, ..) |
| <CRLF\>     |      |    |
| Message-Body | >      |    Optional: Beschreibung der Session SDP oder anderer Protokolle wie ISUP, QSIG oder Hersteller-spezifische Protokolle (Codecs, ..) |
5. In welche Nachrichten muss ein Message-Body enthalten sein und welche Information ist darin enthalten?  
Request und Response.   
Im ersten Teil der Beschreibung werden generelle Aspekte, Rahmenbedingungen und
übergreifende Parameter der gesamten Verbindung dargestellt z.B. v für Version oder m für medium (Port, etc).
6. Über welches Protokoll der Transportschicht erfolgt die Übertragung des SIP-Protokolls und des RTP-Prptokolls (mit kurzer Begründung)?    
UDP, SIP kann auch andere, RTP benutzt nur UDP, weil für Medien-Streams nicht wirklich von großer Bedeutung ist, wenn ab und zu ein paar Pakete nicht ankommen, diese nochmals zu senden (was TCP tut) wäre z.B. bei Live-Telefonaten ein Irrsinn.

### Notizen 
* SIP initialisiert eine Verbindung, dann Peer-to-Peer Kommunikation, Signalisierungsprotokoll, liegt auf Application-Layer, UDP (Bei Anruf sollen Pakete nicht nochmal ankommen), Multimedia-Daten
* RTP, überträgt Audio, Video, Multimedia-Daten, Nutzdaten
* SDP beschreibt Codecs, etc. damit Peer weiß ob er anderen unterstützt
* SIP-Request: Invite, Ack; Cancel zum Ablehnen; Options für Parameter (Codecs, etc); Register für Übermittlung der Standortinformationen
* SIP-Responses: Ähneln HTTP (eg. 404 - not found)
* B2BUA - Server zwischen zwei User-Agents

## Quellenverzeichnis
\[0] Allgemein: <http://wwwlehre.dhbw-stuttgart.de/~srupp/DHBW_Lab_NT/LNT_V6_SIP_Labor.pdf>   
\[1] Telefon VS VoIP: <https://www.nextiva.com/blog/voip-vs-landline.html>  
\[2] Proxy: <https://stackoverflow.com/questions/7155529/how-does-http-proxy-work>  
\[3] Client-Server <https://de.wikipedia.org/wiki/Client-Server-Modell>  
