# GK8.2.1 Kommunikationsdienste
### Kacper Urbaniec | 4AHIT | 26.02.2019

## Ziele

Ziel dieser Übung ist es eine Übersicht über E-Mail-System zu bekommen.

## Voraussetzungen

* Grundlagen Linux und DNS
* Virtualisierungsumgebung Ubuntu 16.04 LTS oder 18.04 LTS

## Aufgabenstellung

Installieren und konfigurieren Sie Mail-Server iRedMail. Das Versenden und Empfangen von Mails muss funktionieren - nicht nur lokal am Server, sondern auch über andere Server (verwenden Sie dazu zum Testen die Server der anderen Gruppen - sie müssen dazu auch entsprechende Domains/Hostnamen konfigurieren, entweder über DNS oder über die Hosts-Datei).

## Implementierung
Zuerst habe ich meine Ubuntu 18 Maschine geupdated.

```bash
sudo apt update;sudo apt upgrade
```

Als nächstes muss man die fully qualified domain name für den Server setzen. Anschließend muss die `/etc/hosts` mit der Domain aktualisiert werden.
```bash
sudo hostnamectl set-hostname mail.syt.tgm.ac.at

sudo vim /etc/hosts
```
Änderung in `/etc/host`:
```bash
127.0.0.1       mail.syt.tgm.ac.at localhost
```

Als nächstes wird der iRedMail installer per wget geholt.
```bash
wget https://bitbucket.org/zhb/iredmail/downloads/iRedMail-0.9.9.tar.bz2
```

Dann muss dar tar-Verzeichnis entpackt werden, dann kann schon der Installer per bash ausgeführt werden:
```bash
tar xvf iRedMail-0.9.9.tar.bz2
cd iRedMail-0.9.7/
chmod +x iRedMail.sh
sudo bash iRedMail.sh
```
Bei allen Anforderungen einfach mit `y` antworten.

![](img/1.png)
![](img/2.png)
![](img/3.png)
![](img/4.png)
![](img/5.png)

Bei einer erfolgreichen Installation kann das Admin-Interface per https://`your-domain`/ireadmin wie z.B. https://mail.syt.tgm.ac.at/iredadmin/ aufgerufen werden.    
Credenitals: <postmaster@syt.tgm.ac.at 1234>
![](img/6.png)
Dort kann man neue User erstellen:
<kurbaniec@syt.tgm.ac.at Password1234>    
![](img/7.png)
![](img/8.png)
<user@syt.tgm.ac.at Password1234>
![](img/10.png)

Zum Testen habe ich auf einer anderen Maschine den Mail-Server zu `/etc/hosts` mit der Domain (`mail.syt.tgm.ac.at`) hinzugefügt, um von dort auch Emails zu senden.

Die Posteingänge können mit https://`your-domain`/mail (https://mail.syt.tgm.ac.at/mail/) aufgerufen werden.

Dort habe ich zwischen Server und einer anderen VMs Mails gesendet:
![](img/11.png)
![](img/12.png)

---

## Einführung
Im Zuge dieser Übung soll eine SIP/VoIP Infrastruktur mit Kamailio Server aufgebaut und konfiguriert werden.

## Voraussetzungen

* Grundlagen SIP, SDP und RTP
* Virtualisierungsumgebung Debian Stretch

## Aufgabenstellung
Aufbau einer VoIP/SIP Infrastruktur:

* Installation und Konfiguration eines SIP Server
* Installation und Konfiguration von SIP User Agenten

Anschließend überprüfen Sie die Konfiguration der SIP-Infrastruktur mittels zwei User-Agents (UAs). Ein Basic Call zwischen zwei UAs soll funktionieren. Für die Übung sind zwei SIP-UAs notwendig, die beispielsweise auf den virtuellen Maschinen installiert werden können.

## Implementierung
### Server
Für die Installation von Kamailio sind folgende Pakete notwendig:
```bash
sudo apt-get install default-mysql-server
sudo apt-get install git-core
sudo apt-get install gcc g++
sudo apt-get install flex
sudo apt-get install bison
sudo apt-get install libmysqlclient-dev
sudo apt-get install make autoconf
sudo apt-get install libssl-dev
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libxml2-dev
sudo apt-get install libpcre3-dev
```

Zu aller sollte man einen Ordner erstellen, in diesem werden per git die Source-Files geholt.   
**Hinweis**: Ich benutzte hier die letzte stable-Version. Die Dev(el)-Version hat bei mir nicht funktioniert, darum würde ich empfehlen stable zu benutzen.

```bash
sudo mkdir -p /usr/local/src/kamailio-5.2
cd cd /usr/local/src/kamailio-5.2
sudo git clone --depth 1 --no-single-branch https://github.com/kamailio/kamailio kamailio
cd kamailio
git checkout -b 5.2 origin/5.2
```

Als nächstes werden die build config files erstellt:
```bash
sudo make cfg
```
Diese müssen editiert werden, um die Datenbank einzutragen:
```bash
sudo vim src/modules.lst
```
Folgende Zeile verfollständigen:
```bash
include_modules= db_mysql dialplan
```

Als nächstes kann alles kompiliert bzw. installiert werden:
```bash
sudo make include_modules="db_mysql dialplan" cfg
sudo make all
sudo make install
```

Der nächste Schritt ist die Datenbank für Kamailio automtisch zu erstellen. Dazu öffnet man folgendes File:
```bash
sudo nano -w /usr/local/etc/kamailio/kamctlrc
```
Dort ergänzt und/bzw. entfernt man das vorgeschobene Kommentar und trägt Domain und Engine ein:
```bash
SIP_DOMAIN=sip.syt.tgm.ac.at
DBENGINE=MYSQL
```
Jetzt sollte die Datenbank generierbar sein:
```bash
/usr/local/sbin/kamdbctl create
```
In MySQL wird jetzt ein neuer User `kamailio` mit dem Passwort `kamailiorw` erstellt, dieses sollte man sich merken, da beim Hinzufügen von Benutzern dieses abgefragt wird.

Als nächstes wird RTPproxy installiert, dies dient dazu da um die Kommunikation zwischen SIP user agents mit NAT zu ermöglichen:
```bash
sudo apt-get install rtpproxy
systemctl start rtpproxy.service
```

RTPproxy muss in Kamailio konfiguriert werden:
```bash
sudo vim /usr/local/etc/kamailio/kamailio.cfg
```
Nach der ersten Zeile (`#!KAMAILIO`) muss folgendes stehen:
```bash
#!define WITH_NAT
```

Jetzt können wir Kamailio ausführen, zuerst sollte man aber ein init.d-Skript erstellen:
```bash
sudo cp /usr/local/src/kamailio-devel/kamailio/pkg/kamailio/deb/debian/kamailio.init /etc/init.d/kamilio
sudo chmod 755 /etc/init.d/kamailio
sudo vim /etc/init.d/kamailio
```
Folgendes sollte im File ergänzt werden:
```bash
DAEMON=/usr/local/sbin/kamailio
CFGFILE=/usr/local/etc/kamailio/kamailio.cfg
```
Jetzt kann Kamilio gestartet/gestoppt werden über:
```bash
sudo /etc/init.d/kamailio start
sudo /etc/init.d/kamailio stop
```
Alternativ:
```bash
sudo systemctl start kamailio.service
sudo systemctl stop kamailio.service
```

Jetzt können wir neue User hinzufügen:
```bash
cd /usr/local/sbin/
kamctl add user1 1234
kamctl add user2 1234
```
Dies erstellt zwei User(`user1`, `user2`), beide mit dem Passwort `1234`. Beim erstellen wird nach dem Passwort `kamailiorw` gefragt.   
**Hinweis**:
Folgendes Hinzufügen von Usern hat keine Fehler erzeugt, ich konnte sie im VoIP-Client nicht benutzen, weiß nicht ob sie wirklich ungültig sind oder das Programm `Linphone` hier einen Fehler hat:    
~~kamctl add 0 1234   
kamctl add 1 1234~~

### Clients

Um den SIP-Server mit VoIP zu testen habe ich auf zwei Ubuntu-VMs das Programm `Linphone` installiert:
```bash
sudo apt-get install linphone
```
Auf beiden Cients, wie auch am Server muss die in der Kamailio-Konfiguration angegebene Domain in `/etc/hosts` eingetragen sein.

Zuerst geht man zu den Einstellungen (Preferences) und geht auf den Register `Manage SIP Accounts`. Dort klickt man auf `Wizard`:
![](img/13.png)
Im Konfigurations-Assistenten wählt man den dritten Punkt (SIP) aus:
![](img/14.png)
Dann trägt man einen User ein, den man am Server ge-added hat. Nochmals zur Erinnerung. Die Domains muss aus Kamailio und der Host-Datei stammen.
![](img/15.png)
Jetzt sollte unten irgendwas von `Successfully registered` stehen. Wenn man dies jetzt auf zwei Clients konfiguriert hat, kann z.B. der User `user2` den User `user2` anrufen:
![](img/16.png)
Der User `user1` kann jetzt den Anruf annehmen mit dem Answer-Button:
![](img/17.png)
Jetzt sollte eine VoIP-Verbindung zwischen den Clients zustande kommen:
![](img/18.png)
![](img/19.png)


# Quellenverzeichnis
\[1] iRedMail <https://www.linuxbabe.com/mail-server/ubuntu-16-04-iredmail-server-installation>   
\[2] Kamailio <https://kamailio.org/docs/tutorials/devel/kamailio-install-guide-git/>   
\[3] Kamailio <https://trinhhieu668.wordpress.com/2017/10/25/kamailio-add-user-and-test-with-softphone-linphone-zoiper/>   
\[4] RTPproxy <https://packages.debian.org/de/sid/rtpproxy>   
