# "*syt4-sommer-semester-kurbaniec-tgm*"

### Middleware Engineering

* [GK8.3.1 Middleware Engineering "Document Oriented Middleware using MongoDB" (MICT)](https://bitbucket.org/kurbaniec/syt4-gk831-document-middleware-mongodb-kurbaniec-tgm)
* [GK8.3.4 Middleware Engineering "Microservices" (MICT-Theorie)](https://bitbucket.org/kurbaniec/syt4-gk834-microservices-kurbaniec-tgm)

### Distributed Computing

* [GK8.3.2 Distributed Computing "Komponentenbasierte Programmierung" (BORM)](https://bitbucket.org/kurbaniec/syt4-gk832-jpa-westbahn-kurbaniec-tgm)
* [GEK8.3.3 Distributed Computing "RMI Task Loadbalancer" (BORM-Theorie)](https://bitbucket.org/kurbaniec/syt4-gk833-rmi-taskloadbalancer-kurbaniec-tgm)
* [GK8.3.5 Middleware Engineering "Webframeworks" (MICT)](https://bitbucket.org/kurbaniec/syt4-gk835-componentbase-ui-kurbaniec-tgm)

### Infrastructure Management

* [GK8.2.1 Kommunikationsdienste (UMAA)](syt4-gk821-kommunikationsdienste)
* [GEK8.2.2 Control in Linux iptables (UMAA-Theorie)](syt4-gk822-control-in-linux-iptables)
* [EK8.2.4 Kommunikationsdienste SIP-Server (UMAA)](syt4-ek824-sip-server)

### Robotics

* [GK8.1.2 Externe Signale und Gerätekommunikation (JANJ)](syt4-gk812-externe-signale-und-geraetekommunikation)
* [GK8.1.3 Offline-Projektierung und Programmierung (JANJ)](syt4-gk813-offline-projektierung-und-programmierung)

### Industrial Programming

* [GEK8.1.4 Industrial Programming "Anzeige und Analyse von Sensordaten" (BORM-Theorie)](https://bitbucket.org/kurbaniec/syt4-gk814-sensordata-urbaniec-wustinger)

### Embedded Devices

* [GK8.1.1 Embedded Devices "USART und SPI" (WEIJ)](syt4-gk811-embedded-devices-USART-und-SPI)